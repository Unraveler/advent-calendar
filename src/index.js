import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { appFunctions } from './functions.js'

class AdventDoor extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            presentStatus:null,
            present: null,
            openDoor: false,
            alreadyOpenedOnce: false,
        };
    }
    onClickDate(value) {
        const date = new Date().getDate();
        if (value <= date && !this.state.openDoor) {
            this.setState({
                openDoor: true
            })
            if (!this.state.alreadyOpenedOnce) {
              appFunctions.getRandomPresent().then(gif => {
                this.setState({present: gif})
              })
            }
            this.setState({
                alreadyOpenedOnce: true
            });
            
        } else if (value <= date && this.state.openDoor) {
            this.setState({
                openDoor: false
            })
        } else {
            alert("not allowed to open yet")
        }
    }
    render() {
      if(this.state.openDoor){
        return(
          <div className = "adventDoor" onClick = {() => this.onClickDate(this.props.value)}> 
            <img src = {this.state.present} alt = "Opening" /> 
          </div>
        )
      }
      else {
        return(
          <div className = "adventDoor" onClick = { () => this.onClickDate(this.props.value)}> 
            {this.props.value} 
          </div>
        )
      }
    }
}

class AdventBoard extends React.Component {
  constructor(props) {
      super(props);
      this.possibleDays = [];
  }
  listDays() {
      let possibleDays = this.possibleDays;
      return appFunctions.randomNumberArray(possibleDays, 1, 24)
  }
  render() {
    const listDays = this.listDays();
    return ( 
      <div className = "adventBoard"> 
        {listDays.map(day => <AdventDoor value = {day} />)} 
      </div>
    );
  }
}

ReactDOM.render( <AdventBoard/> , document.getElementById('root'));