import axios from 'axios';

const appFunctions = {
	randomNumberArray: (possibleDays,min,max) => {
    	while (possibleDays.length < max){
      		let newNumber = Math.floor(Math.random() * max + min)
      		if(possibleDays.indexOf(newNumber)===-1){
        		possibleDays.push(newNumber)  
      		}
    	}
    	return possibleDays;
  	},
  	getRandomPresent: () => {

  		return axios({
  			method:'get',
  			url:'/v1/gifs/random?api_key=5fhHQ0Cm7fqeRd5cdKw2VAYxV4dS012k&tag=&rating=G',
  			baseURL:'https://api.giphy.com',
  			timeout:2000
		})
  			.then((success) => {		
  				const url = success.data.data.image_url;
  				return url
  			})
  			.catch((error) => {
  				alert(error)
  			});
  	}
}

export {appFunctions}