Description
---
Work in progress.

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Giphy API was used to randomly get the gifs behind each door.

Before you begin
---
- You will also need to configure <b>NodeJS</b>, <b>Node Package Manager(npm)</b> and <b>Grunt</b> in your system before starting. If you didn't do it already, run the following commands (on linux distros):
    - <b>NodeJS</b>: `sudo apt-get install nodejs`
    - <b>npm</b>: `sudo apt-get install npm`

Installation
---
- `npm install`
- `npm start`
